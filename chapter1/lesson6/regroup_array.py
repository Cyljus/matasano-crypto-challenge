def regroup_array(array):
    output = []
    for row in range(0,len(array)):
        foo = []
        for element in range(0, len(array[row])):
            foo.append(array[element][row])
        output.append(foo)
    return(output)
def  main():
    test = [
            [1, 2, 3, 4, 5],
            [1, 2, 3, 4, 5],
            [1, 2, 3, 4, 5],
            [1, 2, 3, 4, 5],
            [1, 2, 3, 4, 5]
            ]
    print(regroup_array(test))


if __name__ == '__main__':
    main()
