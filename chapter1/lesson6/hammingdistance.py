import binascii
import argparse

def Main():
    in1 = "this is a test"
    in2 = "wokka wokka!!!"
    print(get_distance(in1, in2))


def get_distance(string1, string2):
    out = 0
    for each in range(0, len(string1)):
        out = out + bin(ord(string1[each]) ^ ord(string2[each])).count("1")
    return out    


if __name__ == '__main__':
    Main()
