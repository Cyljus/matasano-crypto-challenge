import binascii
import argparse


def Main():
	parser = argparse.ArgumentParser()
	parser.add_argument("input", help="Input You want to convert to Base64")
	args = parser.parse_args()
	output = binascii.b2a_base64(binascii.unhexlify(args.input))
	print(output.decode("utf-8"))

if __name__ == '__main__':
	Main()
