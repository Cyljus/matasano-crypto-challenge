import binascii
import argparse

def Main():
    parser = argparse.ArgumentParser()
    parser.add_argument("text", help="Text ypu want to encrypt")
    parser.add_argument("key", help="Key you want to use")
    args = parser.parse_args()
    
    text = []
    key = []
    key_length = 0
    current_key_byte = 0
    output = ""

    for each in args.text:
        text.append(ord(each))
    for each in args.key:
        key.append(ord(each))

    key_length = len(args.key)  
    for each in text:
        output = output + '{:02x}'.format(each ^ key[current_key_byte], 'x')
        current_key_byte += 1
        current_key_byte = current_key_byte % key_length
    
    print(output)
if __name__ == '__main__':
        Main()
