import binascii
import argparse
def Main():
	parser = argparse.ArgumentParser()
	parser.add_argument("input1", help="Hex String nr1 you want to be xored")
	parser.add_argument("input2", help="Hex String nr2 you want to be xored")
	args = parser.parse_args()
	output = [] 
	output = int(args.input1, 16) ^ int(args.input2, 16)
	print (format(output, 'x'))

if __name__ == '__main__':
	Main()