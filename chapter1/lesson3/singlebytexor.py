import binascii
import argparse

def Main():
	cmdoutput = ""	
	parser = argparse.ArgumentParser()
	parser.add_argument("cmdinput", help="Hex String nr1 you want to be xored")
	args = parser.parse_args()
	current_score = 0
	current_xor = 0
	for each in range(255):
		if current_score <= grade_ascii(xor_input(args.cmdinput, each)):
			current_score = grade_ascii(xor_input(args.cmdinput, each))
			current_xor = each
	for each in xor_input(args.cmdinput, current_xor):
		cmdoutput = cmdoutput + chr(each)
	print(cmdoutput)

def xor_input(input1, input2):
    output = []
    for each in range(0, len(input1), 2):
        output.append(int((input1[each] + input1[each+1]), 16) ^ input2)
    input1 = int(input1, 16)
    return output


def grade_ascii(input):#Grade the ammount of ascii characters in the output
    score = 0
    for each in input:
        #kleine buchstaben
        if each >= 97 and each <= 122:
            score += 4
        #große und leer
        elif each >= 65 and each <= 90 or each == 32:
            score += 3
        #Zahlen
        elif each >= 48 and each <= 57:
            score += 2
        #druckbar
        elif each >= 32 and each <= 126:
            score += 1
        else:
            score -= 3
    return score


if __name__ == '__main__':
	Main()
